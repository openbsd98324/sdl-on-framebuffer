
/// It is MIT and GNU, free opensource.
// this will draw on /dev/fb0 using SDL1.2
// This will use a loop to detect keys
// keys to play, u d, up, down, left and right, this will change your colors.
// q: quit 

#include <SDL/SDL.h>                                                                
#include <SDL/SDL_getenv.h>                                                         
#include <stdio.h>
#include <stdlib.h>




/*
   apt-get update ; apt-get install libsdl1.2-dev gcc clang  
   gcc sdl-fb0.c -lSDL  -o /tmp/sdl-test   
   /tmp/sdl-test 

   On Devuan: 
     cc sdl-fb0-1.c -lSDL -o /tmp/sdl
 */





// Define the primary surface for display                                           
SDL_Surface* scrMain = NULL;                                                        

// Main entrypoint                                                                  
int main(int argc, char* args[])                                                    
{                                                                                   
	// --------------------------------------                                   
	// Initialization                                                           
	// --------------------------------------                                   


	// Update the environment variables for SDL to                              
	// work correctly with the external display on                              
	// LINUX frame buffer 1 (fb1).                                              
	putenv((char*)"FRAMEBUFFER=/dev/fb0");                                      
	putenv((char*)"SDL_FBDEV=/dev/fb0");                                        

	// Initialize SDL                                                           
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {                                         
		fprintf(stderr,"ERROR in SDL_Init(): %s\n",SDL_GetError());         
		return 0;                                                           
	}                                                                           




	// Fetch the best video mode                                                
	// - Note that the Raspberry Pi generally defaults                          
	//   to a 16bits/pixel framebuffer                                          
	const SDL_VideoInfo* vInfo = SDL_GetVideoInfo();                            
	if (!vInfo) {                                                               
		fprintf(stderr,"ERROR in SDL_GetVideoInfo(): %s\n",SDL_GetError()); 
		return 0;                                                           
	}                                                                           
	int     nResX = vInfo->current_w;                                           
	int     nResY = vInfo->current_h;                                           
	int     nDepth = vInfo->vfmt->BitsPerPixel;                                 
	printf(    "Resolution test: %d %d %d \n" ,  nResX , nResY , nDepth ); 
	// Configure the video mode                                                 
	// - SDL_SWSURFACE appears to be most robust mode                           
	int     nFlags = SDL_SWSURFACE;                                             
	scrMain = SDL_SetVideoMode(nResX,nResY,nDepth,nFlags);                      
	if (scrMain == 0) {                                                         
		fprintf(stderr,"ERROR in SDL_SetVideoMode(): %s\n",SDL_GetError()); 
		return 0;                                                           
	}                                                                           

	//screen = SDL_SetVideoMode( cols , rows, 32, SDL_HWSURFACE  ); 
	SDL_putenv( "SDL_VIDEO_CENTERED=center"); 
	SDL_Event event;


	// --------------------------------------                                   
	// Perform some simple drawing primitives                                   
	// --------------------------------------                                   

	// Draw a gradient from red to blue                                         
	SDL_Rect        rectTmp;                                                    
	Uint32          nColTmp;                                                    
	for (Uint16 nPosX=0;nPosX<nResX;nPosX++) 
	{                                  
		rectTmp.x = nPosX;                                                  
		rectTmp.y = nResY/2;                                                
		rectTmp.w = 1;                                                      
		rectTmp.h = nResY/2;                                                
		nColTmp = SDL_MapRGB(scrMain->format,nPosX%256,0,255-(nPosX%256));  
		SDL_FillRect(scrMain,&rectTmp,nColTmp);                             
	}                                                                           

	// Set Blue Color and more 
	Uint32 nColBlue   = SDL_MapRGB(scrMain->format,0 ,0 , 255 );   // RVB
	Uint32 nColRed    = SDL_MapRGB(scrMain->format,255 ,0 , 0 );   // RVB
	Uint32 nColYellow = SDL_MapRGB(scrMain->format, 255, 255, 0  ); // RVB

	// Draw a green box                                                         
	Uint32 nColGreen = SDL_MapRGB(scrMain->format,0,255,0);                     
	SDL_Rect rectBox = {0,0,nResX,nResY/2};                                     
	SDL_FillRect(scrMain, &rectBox, nColGreen);                                   

        // define another rect 
	SDL_Rect rectBox2 = {nResX/2,nResY/2,nResX-10,nResY-10};                                     

	// Now that we've completed drawing, update the main display                
	SDL_Flip(scrMain);                                                          

	// Wait for a short delay                                                   
	//SDL_Delay(3000);                                                            


	int gameover = 0;
	while ( gameover == 0 )
	{
		// rects
		//mvrectsdl( 100 , cols-200, 200, cols-10 , screen, 0x00FF00 );
		//mvrectsdl( 400 , cols-200, 500, cols-10 , screen, 0xFF0000 );
		// draw pixels to screen 

		// like getch
		SDL_WaitEvent(&event);

		if ( event.type == SDL_KEYDOWN )
			printf( "Keydown \n" );

		if ( event.type == SDL_KEYDOWN )
			switch( event.key.keysym.sym  )
			{
				case SDLK_ESCAPE:
					printf( "ESC\n" );
					gameover = 1;
					break;
				case SDLK_q:
					printf( "Quit, pressed 'q'\n" );
					gameover = 1;
					break;
				case SDLK_LEFT:
					printf( "LEFT\n" );
					SDL_FillRect(scrMain, &rectBox2, nColBlue);                                   
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_RIGHT:
					printf( "RIGHT\n" );
					SDL_FillRect(scrMain, &rectBox2, nColGreen);
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_DOWN:
				case SDLK_j:
					printf( "DOWN\n" );
					SDL_FillRect(scrMain, &rectBox, nColYellow );
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_UP:
				case SDLK_k:
					printf( "UP\n" );
					SDL_FillRect(scrMain, &rectBox, nColRed );
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_PAGEDOWN:
				case SDLK_d:
					printf( "PAGE DOWN\n" );
					SDL_FillRect(scrMain, &rectBox, nColGreen );
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_PAGEUP:
				case SDLK_u:
					printf( "PAGE UP\n" );
					SDL_FillRect(scrMain, &rectBox, nColBlue);                                   
					SDL_Flip( scrMain ); //refresh
					break;

				case SDLK_HOME:
				case SDLK_g:
					printf( "HOME\n" );
					break;
				case SDLK_RETURN:
					break;
			}
	}

	// Close down SDL                                                           
	SDL_Quit();                                                                 

	return 0;                                                                   
}                                                                                   





// this will draw on framebuffer /dev/fb0 using SDL1.2 (without X11)

#include <SDL/SDL.h>                                                                
#include <SDL/SDL_getenv.h>                                                         
#include <stdio.h>
#include <stdlib.h>

/*
           apt-get update ; apt-get install libsdl1.2-dev gcc clang  
	   gcc sdl-fb0.c -lSDL  -o /tmp/sdl-test   
	   /tmp/sdl-test 
*/


// Define the primary surface for display                                           
SDL_Surface* scrMain = NULL;                                                        

// Main entrypoint                                                                  
int main(int argc, char* args[])                                                    
{                                                                                   
	// --------------------------------------                                   
	// Initialization                                                           
	// --------------------------------------                                   

	// Update the environment variables for SDL to                              
	// work correctly with the external display on                              
	// LINUX frame buffer 1 (fb1).                                              
	putenv((char*)"FRAMEBUFFER=/dev/fb0");                                      
	putenv((char*)"SDL_FBDEV=/dev/fb0");                                        

	// Initialize SDL                                                           
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {                                         
		fprintf(stderr,"ERROR in SDL_Init(): %s\n",SDL_GetError());         
		return 0;                                                           
	}                                                                           

	// Fetch the best video mode                                                
	// - Note that the Raspberry Pi generally defaults                          
	//   to a 16bits/pixel framebuffer                                          
	const SDL_VideoInfo* vInfo = SDL_GetVideoInfo();                            
	if (!vInfo) {                                                               
		fprintf(stderr,"ERROR in SDL_GetVideoInfo(): %s\n",SDL_GetError()); 
		return 0;                                                           
	}                                                                           
	int     nResX = vInfo->current_w;                                           
	int     nResY = vInfo->current_h;                                           
	int     nDepth = vInfo->vfmt->BitsPerPixel;                                 

	printf(    "Resolution test: %d %d %d \n" ,  nResX , nResY , nDepth ); 


	// Configure the video mode                                                 
	// - SDL_SWSURFACE appears to be most robust mode                           
	int     nFlags = SDL_SWSURFACE;                                             
	scrMain = SDL_SetVideoMode(nResX,nResY,nDepth,nFlags);                      
	if (scrMain == 0) {                                                         
		fprintf(stderr,"ERROR in SDL_SetVideoMode(): %s\n",SDL_GetError()); 
		return 0;                                                           
	}                                                                           


	// --------------------------------------                                   
	// Perform some simple drawing primitives                                   
	// --------------------------------------                                   

	// Draw a gradient from red to blue                                         
	SDL_Rect        rectTmp;                                                    
	Uint32          nColTmp;                                                    
	for (Uint16 nPosX=0;nPosX<nResX;nPosX++) {                                  
		rectTmp.x = nPosX;                                                  
		rectTmp.y = nResY/2;                                                
		rectTmp.w = 1;                                                      
		rectTmp.h = nResY/2;                                                
		nColTmp = SDL_MapRGB(scrMain->format,nPosX%256,0,255-(nPosX%256));  
		SDL_FillRect(scrMain,&rectTmp,nColTmp);                             
	}                                                                           

	// Draw a green box                                                         
	Uint32 nColGreen = SDL_MapRGB(scrMain->format,0,255,0);                     
	SDL_Rect rectBox = {0,0,nResX,nResY/2};                                     
	SDL_FillRect(scrMain,&rectBox,nColGreen);                                   


	// Now that we've completed drawing, update the main display                
	SDL_Flip(scrMain);                                                          

	// Wait for a short delay                                                   
	SDL_Delay(3000);                                                            

	// Close down SDL                                                           
	SDL_Quit();                                                                 

	return 0;                                                                   
}                                                                                   





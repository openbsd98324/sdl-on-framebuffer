# sdl-on-framebuffer
 
![](sdl-2d-game.png)


## Compilation

`cd SDL ; make  `

## References 
PI: https://www.raspberrypi.org/

Games: https://retropie.org.uk/


## LIBS With LIB Config


````
-I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT
-L/usr/lib/arm-linux-gnueabihf -lSDL
````

````
        echo it runs on retropie with 4.5, without use of sudo.
	g++ `sdl-config --cflags --libs` game03.cpp -o /tmp/sdlanim  
	/tmp/sdlanim 
````




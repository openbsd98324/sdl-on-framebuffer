
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>                                                                
#include <SDL/SDL_getenv.h>                                                         

/*
   apt-get update ; apt-get install libsdl1.2-dev gcc clang  

game:
	g++ `sdl-config --cflags --libs` game02.cpp -o /tmp/sdlanim  
	/tmp/sdlanim 
*/

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480
#define SPRITE_SIZE   32

int gameover;

/* source and destination rectangles */
SDL_Rect rcSrc,  rcSprite;    // player 1


void HandleEvent(SDL_Event event)
{
	switch (event.type) {
		/* close button clicked */
		case SDL_QUIT:
			gameover = 1;
			break;
			

		/* handle the keyboard */
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					gameover = 1;
					break;


                                // Player 
				case SDLK_h:
					if ( rcSrc.x == 192 )
						rcSrc.x = 224;
					else
						rcSrc.x = 192;
					rcSprite.x -= 5;
					break;

				case SDLK_l:
					if ( rcSrc.x == 64 )
						rcSrc.x = 96;
					else
						rcSrc.x = 64;
					rcSprite.x += 5;
					break;

				case SDLK_k:
					if ( rcSrc.x == 0 )
						rcSrc.x = 32;
					else
						rcSrc.x = 0;
					rcSprite.y -= 5;
					break;

				case SDLK_j:
					if ( rcSrc.x == 128 )
						rcSrc.x = 160;
					else
						rcSrc.x = 128;
					rcSprite.y += 5;
					break;


			}
			break;
	}
}







int main(int argc, char* argv[])
{

	putenv((char*)"FRAMEBUFFER=/dev/fb0");                                      
	putenv((char*)"SDL_FBDEV=/dev/fb0");                                        

        // Define the primary surface for display                                           
	SDL_Surface *screen, *temp, *sprite, *grass, *ghost, *monster, *minotaur;
	SDL_Rect rcGrass;
	int colorkey;

	/* initialize SDL */
	SDL_Init(SDL_INIT_VIDEO);

	/* set the title bar */
	SDL_WM_SetCaption("SDL Animation", "SDL Animation");

	const SDL_VideoInfo* vInfo = SDL_GetVideoInfo();                            
	int     nResX = vInfo->current_w;                                           
	int     nResY = vInfo->current_h;                                           
	int     nDepth = vInfo->vfmt->BitsPerPixel;                                 

	printf(    "We are now on Framebuffer /dev/fb0 ... (use pi, /home/pi on retropie).\n" );
	printf(    "Resolution test: %d %d %d \n" ,  nResX , nResY , nDepth ); 

	int     nFlags = SDL_SWSURFACE;                                             
	screen = SDL_SetVideoMode(nResX,nResY,nDepth,nFlags);                      

	/* set keyboard repeat */
	SDL_EnableKeyRepeat( 70, 70 );

	/* load sprite */
	temp   = SDL_LoadBMP("sprite.bmp");
	sprite = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load grass */
	temp  = SDL_LoadBMP("grass.bmp");
	grass = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 255, 0, 255);
	SDL_SetColorKey(sprite, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);



	/* set sprite position */
	rcSprite.x = 150;
	rcSprite.y = 150;


	/* set animation frame */
	rcSrc.x = 128;
	rcSrc.y = 0;
	rcSrc.w = SPRITE_SIZE;
	rcSrc.h = SPRITE_SIZE;





	gameover = 0;
	/* message pump */
	while (!gameover)
	{
		SDL_Event event;
		
		/* look for an event */
		if (SDL_PollEvent(&event)) {
			HandleEvent(event);
		}

		/* collide with edges of screen */
		if (rcSprite.x <= 0)
			rcSprite.x = 0;
		if (rcSprite.x >= SCREEN_WIDTH - SPRITE_SIZE) 
			rcSprite.x = SCREEN_WIDTH - SPRITE_SIZE;

		if (rcSprite.y <= 0)
			rcSprite.y = 0;
		if (rcSprite.y >= SCREEN_HEIGHT - SPRITE_SIZE) 
			rcSprite.y = SCREEN_HEIGHT - SPRITE_SIZE;

		/* draw the grass */
		for (int x = 0; x < SCREEN_WIDTH / SPRITE_SIZE; x++) {
			for (int y = 0; y < SCREEN_HEIGHT / SPRITE_SIZE; y++) {
				rcGrass.x = x * SPRITE_SIZE;
				rcGrass.y = y * SPRITE_SIZE;
				SDL_BlitSurface(grass, NULL, screen, &rcGrass);
			}
		}


		/* draw the sprite */
		SDL_BlitSurface( sprite, &rcSrc, screen, &rcSprite);

		/* update the screen */
		SDL_UpdateRect( screen, 0, 0, 0, 0 );
	}

	/* clean up */
	SDL_FreeSurface(sprite);
	SDL_FreeSurface(grass);
	SDL_Quit();

	return 0;
}





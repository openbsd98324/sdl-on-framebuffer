
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "SDL.h"
// #include <SDL/SDL.h>


int main(int argc, char *argv[]) 
{
    int gameover = 1;
    SDL_Event event;

    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_WM_SetCaption( "SDL Hello World!", NULL);
    SDL_SetVideoMode( 800, 600, 32, SDL_HWSURFACE);
    int keypress = 0; 

    while (gameover) 
    {
        SDL_WaitEvent(&event);
        printf( " Wait for event...\n"); 

        if ( event.type == SDL_QUIT )
	{
	    printf( " Func SDL Quit \n" ); 
            gameover = 0;
	}

	/* handle the keyboard */
        else if ( SDL_KEYDOWN )
	{
		keypress = event.key.keysym.sym; 
                printf( " Func Key %d \n",  keypress ); 
		if ( keypress == SDLK_q ) 
                   printf( " Func Key 'q' has been pressed.\n");
	}

    }
    SDL_Quit();
    return 0;
}




